define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'mage/url',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'uiRegistry',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/action/redirect-on-success',
        'Godogi_VisaMasterAlrajhi/js/creditcardvalidator'
    ],
    function ($, Component, url, selectPaymentMethodAction, customerData, errorProcessor, fullScreenLoader, checkoutData, quote, checkoutDataResolver, registry, additionalValidators, redirectOnSuccessAction) {
        'use strict';
        return Component.extend({
            redirectAfterPlaceOrder: true, //This is important, so the customer isn't redirected to success.phtml by default
            defaults: {
                template: 'Godogi_VisaMasterAlrajhi/payment/visamasteralrajhi'
            },
            getEmail: function () {
                if(quote.guestEmail) return quote.guestEmail;
                else return window.checkoutConfig.customerData.email;
            },
            /**
             * Place order.
             */
            placeOrder: function (data, event) {
              var dataForm = jQuery('#visamasteralrajhi-form');
              var self = this;
              dataForm.mage('validation', {});
              dataForm.validation();
              if(dataForm.validation('isValid')){
                  if (event) {
                      event.preventDefault();
                  }
                  if (this.validate() && additionalValidators.validate() && this.isPlaceOrderActionAllowed() === true) {
                      this.isPlaceOrderActionAllowed(false);
                      var custom_controller_url = url.build('visamasteralrajhi/visamasteralrajhi/postdata');
                      $.ajax( custom_controller_url , // create checkout on VisaMasterAlrajhi server
                      {
                          type: 'POST',
                          dataType: 'json',
                          showLoader: true,
                          data: {
                              'email': self.getEmail(),
                              'paymentBrand': $("#paymentBrandVisaMasterAlrajhi").val(),
                              'cardHolder': $("#cardHolderVisaMasterAlrajhi").val(),
                              'cardNumber': $("#cardNumberVisaMasterAlrajhi").val().replace(/\s/g, ''),
                              'cardExpiryMonth': $("#cardExpiryMonthVisaMasterAlrajhi").val(),
                              'cardExpiryYear': $("#cardExpiryYearVisaMasterAlrajhi").val(),
                              'cardCvv': $("#cardCvvVisaMasterAlrajhi").val()
                          },
                          success: function (data, status, xhr) {
                              if(data.result){
                                  if(data.result[0].status == 1){
                                      console.log("returned result status 1");
                                      console.log(data);
                                      var result = data.result[0].result;
                                      var index = result.indexOf(":");
                                      var id = result.substr(0, index);
                                      var url = result.substr(index + 1);
                                      window.location.href = url;
                                  } else if(data.result[0].errorText){
                                      console.log("returned error text");
                                      console.log(data);
                                      var errorText = data.result[0].errorText;
                                      $('#visamasteralrajhi-errors').empty();
                                      $('#visamasteralrajhi-errors').append('<p class="title">'+ $.mage.__('Please solve the following inputs') +':</p>');
                                      var errorHtml = '';
                                      errorHtml += '<ul>';
                                      errorHtml += '<li>'+ $.mage.__(errorText) +'</li>';
                                      errorHtml += '</ul>';
                                      $('#visamasteralrajhi-errors').append(errorHtml);
                                      $('#visamasteralrajhi-errors').show(200);
                                      $('html, body').animate({
                                          scrollTop: $("#visamasteralrajhi-errors").offset().top
                                      }, 500);
                                  }
                              } else {
                                  console.log("didn't return anything!");
                                  console.log(data);
                                  $('#visamasteralrajhi-errors').empty();
                                  $('#visamasteralrajhi-errors').append('<p class="title">'+ $.mage.__('Please solve the following inputs') +':</p>');
                                  var errorHtml = '';
                                  errorHtml += '<ul>';
                                  errorHtml += '<li>'+ $.mage.__('Something strange happened, please contact support.') +'</li>';
                                  errorHtml += '</ul>';
                                  $('#visamasteralrajhi-errors').append(errorHtml);
                                  $('#visamasteralrajhi-errors').show(200);
                                  $('html, body').animate({
                                      scrollTop: $("#visamasteralrajhi-errors").offset().top
                                  }, 500);
                              }
                              self.isPlaceOrderActionAllowed(true);
                          },
                          error: function (jqXhr, textStatus, errorMessage) {
                              console.log(jqXhr);
                              return false;
                          }
                      });
                  }
                  return false;
              }else{
                  return false;
              }
            },
            /**
             * After place order callback
             */
            afterPlaceOrder: function () {

            },
            /**
             * Initialize view.
             *
             * @return {exports}
             */
            initialize: function () {
                $( document ).ready(function() {
                    var existCondition = setInterval(function() {
                       if ($('#paymentBrandVisaMasterAlrajhi').length) {
                          clearInterval(existCondition);
                          $('#cardNumberVisaMasterAlrajhi').on('input', function() {
                              $('#cardNumberVisaMasterAlrajhi').removeClass('shake');
                              var input = $('#cardNumberVisaMasterAlrajhi').val().split('');
                              var arrayLength = input.length;
                              var output = [];
                              var shake = false;
                              var allowedCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
                              for (var i = 0; i < arrayLength; i++) {
                                  if (allowedCharacters.includes(input[i])) {
                                      output.push(input[i]);
                                  } else {
                                      shake = true;
                                  }
                              }
                              if(shake){
                                $('#cardNumberVisaMasterAlrajhi').addClass('shake');
                              }

                              var outputString = output.join("");
                              $('#cardNumberVisaMasterAlrajhi').val(outputString);

                              var creditCardNumberVal = $('#cardNumberVisaMasterAlrajhi').val();
                              creditCardNumberVal = creditCardNumberVal.replace(/\s/g, '');
                              if(creditCardNumberVal.match(/^-{0,1}\d+$/)){
                                  if(creditCardNumberVal.length > 16 ){
                                      var output = [];
                                      var input = $('#cardNumberVisaMasterAlrajhi').val().split('');
                                      for (var i = 0; i < 16; i++) {
                                          output.push(input[i]);
                                      }
                                      $('#cardNumberVisaMasterAlrajhi').addClass('shake');
                                      var outputString = output.join("");
                                      $('#cardNumberVisaMasterAlrajhi').val(outputString);
                                  }
                              }

                              $('#cardNumberVisaMasterAlrajhi').validateCreditCard(function(result) {


                                  if(result.card_type){
                                      if(result.card_type.name == 'visa'){
                                          var newBrand = 'visa';
                                          var imgSrc = $( "#paymentBrandVisaMasterImgAlrajhi" ).attr('src');
                                          var newImgSrc = imgSrc.replace(/(.*)\/.*(\.png$)/i, '$1/'+newBrand+'$2');
                                          $( "#paymentBrandVisaMasterImgAlrajhi" ).attr('src', newImgSrc);
                                          $( "#paymentBrandVisaMasterAlrajhi" ).val('VISA');
                                      }else if(result.card_type.name == 'mastercard'){
                                          var newBrand = 'master';
                                          var imgSrc = $( "#paymentBrandVisaMasterImgAlrajhi" ).attr('src');
                                          var newImgSrc = imgSrc.replace(/(.*)\/.*(\.png$)/i, '$1/'+newBrand+'$2');
                                          $( "#paymentBrandVisaMasterImgAlrajhi" ).attr('src', newImgSrc);
                                          $( "#paymentBrandVisaMasterAlrajhi" ).val('MASTER');
                                      }else{
                                          var newBrand = 'unknown';
                                          var imgSrc = $( "#paymentBrandVisaMasterImgAlrajhi" ).attr('src');
                                          var newImgSrc = imgSrc.replace(/(.*)\/.*(\.png$)/i, '$1/'+newBrand+'$2');
                                          $( "#paymentBrandVisaMasterImgAlrajhi" ).attr('src', newImgSrc);
                                          $( "#paymentBrandVisaMasterAlrajhi" ).val('NONE');
                                      }
                                  }else{
                                      var newBrand = 'unknown';
                                      var imgSrc = $( "#paymentBrandVisaMasterImgAlrajhi" ).attr('src');
                                      var newImgSrc = imgSrc.replace(/(.*)\/.*(\.png$)/i, '$1/'+newBrand+'$2');
                                      $( "#paymentBrandVisaMasterImgAlrajhi" ).attr('src', newImgSrc);
                                      $( "#paymentBrandVisaMasterAlrajhi" ).val('NONE');
                                  }
                                  if(result.valid){
                                      $('#cardNumberVisaMasterAlrajhi').addClass('valid');
                                      $('#cardNumberVisaMasterAlrajhi').removeClass('unvalid');
                                  }else{
                                      $('#cardNumberVisaMasterAlrajhi').addClass('unvalid');
                                      $('#cardNumberVisaMasterAlrajhi').removeClass('valid');
                                  }
                              });
                          });

                          $('#cardCvvVisaMasterAlrajhi').on('input', function() {
                              $('#cardCvvVisaMasterAlrajhi').removeClass('shake');
                              var input = $('#cardCvvVisaMasterAlrajhi').val().split('');
                              var arrayLength = input.length;
                              var output = [];
                              var shake = false;
                              var allowedCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
                              for (var i = 0; i < arrayLength; i++) {
                                  if (allowedCharacters.includes(input[i])) {
                                      output.push(input[i]);
                                  } else {
                                      shake = true;
                                  }
                              }
                              if(shake){
                                $('#cardCvvVisaMasterAlrajhi').addClass('shake');
                              }

                              var outputString = output.join("");
                              $('#cardCvvVisaMasterAlrajhi').val(outputString);

                              var creditCardNumberVal = $('#cardCvvVisaMasterAlrajhi').val();
                              creditCardNumberVal = creditCardNumberVal.replace(/\s/g, '');
                              if(creditCardNumberVal.match(/^-{0,1}\d+$/)){
                                  if(creditCardNumberVal.length > 4 ){
                                      var output = [];
                                      var input = $('#cardCvvVisaMasterAlrajhi').val().split('');
                                      for (var i = 0; i < 4; i++) {
                                          output.push(input[i]);
                                      }
                                      $('#cardCvvVisaMasterAlrajhi').addClass('shake');
                                      var outputString = output.join("");
                                      $('#cardCvvVisaMasterAlrajhi').val(outputString);
                                  }
                              }

                          });

                       }
                    }, 100);
                });
                var billingAddressCode,
                    billingAddressData,
                    defaultAddressData;
                this._super().initChildren();
                quote.billingAddress.subscribe(function (address) {
                    this.isPlaceOrderActionAllowed(address !== null);
                }, this);
                checkoutDataResolver.resolveBillingAddress();
                billingAddressCode = 'billingAddress' + this.getCode();
                registry.async('checkoutProvider')(function (checkoutProvider) {
                    defaultAddressData = checkoutProvider.get(billingAddressCode);
                    if (defaultAddressData === undefined) {
                        // Skip if payment does not have a billing address form
                        return;
                    }
                    billingAddressData = checkoutData.getBillingAddressFromData();
                    if (billingAddressData) {
                        checkoutProvider.set(
                            billingAddressCode,
                            $.extend(true, {}, defaultAddressData, billingAddressData)
                        );
                    }
                    checkoutProvider.on(billingAddressCode, function (providerBillingAddressData) {
                        checkoutData.setBillingAddressFromData(providerBillingAddressData);
                    }, billingAddressCode);
                });
                return this;
            }
        });
    }
);
