<?php
namespace Godogi\VisaMasterAlrajhi\Controller\VisaMasterAlrajhi;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface as TransactionBuilder;
use Magento\Sales\Model\Order\Payment\Transaction;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;

use Godogi\VisaMasterAlrajhi\Helper\Data as AlrajhiHelper;

class CheckPayment extends \Magento\Framework\App\Action\Action implements CsrfAwareActionInterface
{
    protected $_resultPageFactory;
    protected $_scopeConfig;
    protected $checkoutSession;
    protected $orderRepository;
    protected $_transactionBuilder;
    protected $_quoteManagement;
    protected $paymentFactory;
    protected $customerFactory;
    protected $customerRepository;
    protected $_storeManager;
    protected $_customerSession;
    protected $_eventManager;
    protected $_alrajhiHelper;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        TransactionBuilder $transactionBuilder,
        \Magento\Quote\Api\CartManagementInterface $quoteManagement,
        \Magento\Quote\Model\Quote\PaymentFactory $paymentFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        AlrajhiHelper $alrajhiHelper)
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
        $this->orderRepository = $orderRepository;
        $this->_transactionBuilder = $transactionBuilder;
        $this->_quoteManagement = $quoteManagement;
        $this->paymentFactory = $paymentFactory;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->_eventManager = $eventManager;
        $this->_alrajhiHelper = $alrajhiHelper;
        parent::__construct($context);
    }
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/alrajhi.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('=== Check Payment ===');
        $terminalResourceKey = $this->_scopeConfig->getValue('payment/visamasteralrajhi/terminalresourcekey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $customerEmailAddress = $this->_customerSession->getCustomerEmailAddress();
        try {
            $postedData = $this->getRequest()->getPostValue();

            $paymentId = $postedData['paymentid'];
            $trackidId = $postedData['trackid'];
            $tranId = $postedData['tranid'];
            $tranData = $postedData['trandata'];

            $decryptedTranData = $this->_alrajhiHelper->decryptAES($tranData, $terminalResourceKey);

            $urlEncoded = urldecode($decryptedTranData);

            $jsonDecoded = json_decode($urlEncoded);

            $firstElement = $jsonDecoded[0];

            if(isset($firstElement->result) && $firstElement->result == "CAPTURED"){

                $quote = $this->checkoutSession->getQuote();
                $store = $this->_storeManager->getStore();
                $websiteId = $this->_storeManager->getStore()->getWebsiteId();
                $customer = $this->customerFactory->create();
                $customer->setWebsiteId($websiteId);
                $billingAdress = $quote->getBillingAddress();
                $customer->loadByEmail($billingAdress->getEmail());
                if ($customer->getId()) {
                    if(!$this->_customerSession->isLoggedIn()){
                        $quote->setCustomerFirstname($billingAdress->getFirstname());
                        $quote->setCustomerLastname($billingAdress->getLastname());
                        $quote->setCustomerEmail($customerEmailAddress);
                        $quote->setCustomerIsGuest(true);
                    }
                } else {
                    $quote->setCustomerFirstname($billingAdress->getFirstname());
                    $quote->setCustomerLastname($billingAdress->getLastname());
                    $quote->setCustomerEmail($customerEmailAddress);
                    $quote->setCustomerIsGuest(true);
                }
                //$quote->getPayment()->importData(['method' => 'visamasteralrajhi']);
                $payment = $this->paymentFactory->create();
                $payment->setMethod('visamasteralrajhi');
                $quote->setPayment($payment);

                $quote->save();
                $quote->setCustomerEmail($customerEmailAddress);
                $this->_eventManager->dispatch('checkout_submit_before', ['quote' => $quote]);
                $order = $this->_quoteManagement->submit($quote);

                $payment = $order->getPayment();
                $formatedPrice = $order->getBaseCurrency()->formatTxt($order->getGrandTotal());
                $transaction = $this->_transactionBuilder->setPayment($payment)
                    ->setOrder($order)
                    ->setTransactionId($tranId)
                    ->setFailSafe(true)
                    ->build(Transaction::TYPE_CAPTURE);
                $payment->addTransactionCommentsToOrder($transaction, __('The authorized amount is %1.', $formatedPrice));
                $payment->save();
                $transaction->save();
                $resultRedirect = $this->resultRedirectFactory->create();
                $this->checkoutSession->setLastSuccessQuoteId($order->getQuoteId());
                $this->checkoutSession->setLastQuoteId($order->getQuoteId());
                $this->checkoutSession->setLastOrderId($order->getEntityId());
                $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
                $this->_eventManager->dispatch('checkout_submit_all_after', ['order' => $order, 'quote' => $quote]);
                $logger->info($order->getEntityId());
                $logger->info('SUCCESS');
                $resultRedirect->setPath('checkout/onepage/success');
                return $resultRedirect;
            } else {
                /*
                  $jsonDecoded
                  Array
                  (
                      [0] => stdClass Object
                          (
                              [errorText] => !ERROR!-IPAY0100265-PARes status not sucessful.
                              [paymentId] => 600202104981899347
                              [trackId] => XBXUV-MS-0014260
                              [error] => IPAY0100265
                              [paymentTimestamp] => 2021-02-18T08:16:44.700Z
                          )

                  )
                */
                if(isset($firstElement->errorText)){
                    $this->messageManager->addError(__($firstElement->errorText));
                    $logger->info($firstElement->errorText);
                    if(isset($firstElement->trackId)){
                        $logger->info(' - Trans ID: ' .$firstElement->trackId);
                    }
                }
                
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('checkout/cart');
                
                return $resultRedirect;
            }

        } catch (\Exception $e) {
            $logger->info('--- Check Payment Exception ---');
            $logger->info($e->getMessage());
            $this->messageManager->addError(__($e->getMessage()));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('checkout/cart');
            return $resultRedirect;
        }
    }
    public function createCsrfValidationException(RequestInterface $request): ? InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ? bool
    {
        return true;
    }
}
