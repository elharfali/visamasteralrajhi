<?php
namespace Godogi\VisaMasterAlrajhi\Controller\VisaMasterAlrajhi;

use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Godogi\VisaMasterAlrajhi\Helper\Data as AlrajhiHelper;

class PostData extends \Magento\Framework\App\Action\Action
{
    protected $_logger;
    protected $_cart;
    protected $_checkoutSession;
    protected $_order;
    protected $_customerSession;
    protected $_storeManager;
    protected $_resultJsonFactory;
    protected $_scopeConfig;
    protected $_alrajhiHelper;

    public function __construct(
        Context $context,
        \Psr\Log\LoggerInterface $logger,
        Session $checkoutSession,
        Order $order,
        CustomerSession $customerSession,
        Cart $cart,
        JsonFactory $resultJsonFactory,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        AlrajhiHelper $alrajhiHelper
    ){
        parent::__construct($context);
        $this->_logger = $logger;
        $this->_checkoutSession = $checkoutSession;
        $this->_order = $order;
        $this->_customerSession = $customerSession;
        $this->_cart = $cart;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_alrajhiHelper = $alrajhiHelper;
    }

    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/alrajhi.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        try {
            $logger->info('=== Post Data Execute ====================================');
            $result = json_decode($this->request(), true);
            $post_data = array('result' => $result);
            $result = $this->_resultJsonFactory->create();
            return $result->setData($post_data);
        } catch (\Exception $e) {
          $logger->info('*** Post Data Execute Exception***');
          $logger->info($e->getMessage());
        }
    }

    public function request() {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/alrajhi.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('=== Post Data Request ===');
        try {
            $order = $this->_checkoutSession->getLastRealOrder();
            $lastOrder = $this->_order->getCollection()->getLastItem();

            $lastOrderId = $lastOrder->getIncrementId();
            if (strpos($lastOrderId, '-') !== false) {
                $orderIncrementId = explode("-", $lastOrderId);
                $orderIncrementId = $orderIncrementId[0]."-".sprintf('%07d', ($orderIncrementId[1] + 1));
            } else {
                $orderIncrementId = $lastOrderId + 1 ;
            }
            $logger->info($orderIncrementId);

            $grandTotal = $this->_cart->getQuote()->getGrandTotal();
            $currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();

            $transportalId = $this->_scopeConfig->getValue('payment/visamasteralrajhi/transportalid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $transportalPassword = $this->_scopeConfig->getValue('payment/visamasteralrajhi/transportalpassword', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $terminalResourceKey = $this->_scopeConfig->getValue('payment/visamasteralrajhi/terminalresourcekey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            //$url = 'https://securepayments.alrajhibank.com.sa/pg/payment/tranportal.htm';
            $url = $this->_scopeConfig->getValue('payment/visamasteralrajhi/endpointurl', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $randomString = $this->_alrajhiHelper->generateRandomString(5);

            $trandata = [
                              //Mandatory Parameters
                              "amt"           =>  number_format((float)$grandTotal, 2, '.', ''),            // Transaction amount
                              "action"        =>  "1",
                                                                      //  It defines the transactions actions
                                                                      //  Purchase: 1
                                                                      //  Refund: 2
                                                                      //  Void: 3
                                                                      //  Inquiry: 8
                              "password"      =>  $transportalPassword,  // Tranportal password. Merchant download the same in merchant portal.
                              "id"            =>  $transportalId,  // Tranportal ID. Merchant download the same in merchant portal
                              "currencyCode"  =>  "682",              // 3-digit currency code of KSA. Ex:682 .... find here: https://www.iban.com/currency-codes
                              "trackId"       =>  $randomString . "-" .$orderIncrementId,        // Merchant unique reference no
                              "expYear"       =>  $this->getRequest()->getPostValue('cardExpiryYear'),
                              "expMonth"      =>  $this->getRequest()->getPostValue('cardExpiryMonth'),
                              "member"        =>  $this->getRequest()->getPostValue('cardHolder'),
                              "cvv2"          =>  $this->getRequest()->getPostValue('cardCvv'),
                              "cardNo"        =>  $this->getRequest()->getPostValue('cardNumber'),
                              "cardType"      =>  "C",                // Card type Ex : Credit card – C, Debit Card – D
                              "responseURL"   =>  $this->_storeManager->getStore()->getBaseUrl() ."visamasteralrajhi/visamasteralrajhi/checkpayment/",
                              "errorURL"      =>  $this->_storeManager->getStore()->getBaseUrl() ."visamasteralrajhi/visamasteralrajhi/checkpayment/"
                      ];

            $this->_customerSession->setCustomerEmailAddress($this->getRequest()->getPostValue('email'));

            $trandata = '[' . json_encode($trandata) . ']';

            $encryptedTrandata = $this->_alrajhiHelper->encryptAES($trandata, $terminalResourceKey);

            $request =    [
                            	"id"             =>  $transportalId,  // Tranportal ID. Merchant can download the Tranportal id from Merchant portal
                            	"trandata"       =>  $encryptedTrandata,
                          	  "responseURL"    =>  $this->_storeManager->getStore()->getBaseUrl() ."visamasteralrajhi/visamasteralrajhi/checkpayment/",
                            	"errorURL"       =>  $this->_storeManager->getStore()->getBaseUrl() ."visamasteralrajhi/visamasteralrajhi/checkpayment/"
                          ];
            $request = '[' . json_encode($request) . ']';
            $logger->info('test 06');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

            $headers = array();
            $headers[] = 'Accept: application/json';
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $logger->info('test 12');
            $responseData = curl_exec($ch);
            /*
            if (curl_errno($ch)) {
                $logger->info('test 14');
                return $ch;
                return curl_error($ch);
            }
            */
            if($errno = curl_errno($ch)) {
                $logger->info('test 14');
                $error_message = curl_strerror($errno);
                $logger->info($errno);
                $logger->info($error_message);
                return "cURL error (" . $errno . "): " . $error_message ;
            }
            curl_close($ch);
            $logger->info('test 16');
            return $responseData;
        } catch (\Exception $e) {
            $logger->info('*** Post Data Request Exception***');
            $logger->info($e->getMessage());
        }
    }

}
