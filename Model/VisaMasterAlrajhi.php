<?php
namespace Godogi\VisaMasterAlrajhi\Model;

/**
 * Pay In Store payment method model
 */
class VisaMasterAlrajhi extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'visamasteralrajhi';
}
