<?php
namespace Godogi\VisaMasterAlrajhi\Model;

class VisaMasterAlrajhiConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    public function getConfig()
    {
        return [
            'key' => 'visamasteralrajhi'
        ];
    }
}
